# Misra end of processing detection

## Instrumentation
1. `docker build -t misra_detect .`
2. `docker run -it -v "$PWD"/src:/app/src misra_detect bash`
3. `mpirun -np 4 --allow-run-as-root bundle exec ruby src/run.rb`
