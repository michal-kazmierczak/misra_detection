FROM ruby:2.7.0-slim

WORKDIR /app

RUN apt-get update -y && \
    apt-get install -y make libopenmpi-dev openmpi-bin openmpi-common openmpi-doc

COPY Gemfile Gemfile.lock ./

RUN BUNDLE_FORCE_RUBY_PLATFORM=1 bundle install --path=vendor

ADD . ./

WORKDIR /app
