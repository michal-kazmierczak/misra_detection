## Example execution:
```bash
root@575db5084369:/app# mpirun -np 4 --allow-run-as-root bundle exec ruby src/run.rb

I, [2020-03-02T21:36:11.613245 #1236]  INFO -- : -> rank:0 sending activation packet to 1
I, [2020-03-02T21:36:11.613338 #1236]  INFO -- : -> rank:0 sending activation packet to 2
I, [2020-03-02T21:36:11.613436 #1236]  INFO -- : -> rank:0 sending activation packet to 3
I, [2020-03-02T21:36:11.613516 #1236]  INFO -- : -> rank:0 sending token to 1 with 0 count
I, [2020-03-02T21:36:11.613568 #1236]  INFO -- : -> rank:0 sending token to 2 with 0 count
I, [2020-03-02T21:36:11.613620 #1236]  INFO -- : -> rank:0 sending token to 3 with 0 count
I, [2020-03-02T21:36:11.613464 #1237]  INFO -- : ---> rank:1 received packet from 0
I, [2020-03-02T21:36:11.613624 #1238]  INFO -- : -----> rank:2 received packet from 0
I, [2020-03-02T21:36:11.613538 #1239]  INFO -- : -------> rank:3 received packet from 0
I, [2020-03-02T21:36:11.614087 #1239]  INFO -- : -------> rank:3 received token 0 from 0
I, [2020-03-02T21:36:11.614146 #1239]  INFO -- : -------> rank:3 waiting until processing is finished
I, [2020-03-02T21:36:11.614111 #1238]  INFO -- : -----> rank:2 received token 0 from 0
I, [2020-03-02T21:36:11.614202 #1238]  INFO -- : -----> rank:2 waiting until processing is finished
I, [2020-03-02T21:36:11.614491 #1239]  INFO -- : -------> rank:3 starts processing for: 3 seconds
I, [2020-03-02T21:36:11.614498 #1238]  INFO -- : -----> rank:2 starts processing for: 4 seconds
I, [2020-03-02T21:36:11.615679 #1237]  INFO -- : ---> rank:1 received token 0 from 0
I, [2020-03-02T21:36:11.615748 #1237]  INFO -- : ---> rank:1 waiting until processing is finished
I, [2020-03-02T21:36:11.615840 #1237]  INFO -- : ---> rank:1 starts processing for: 1 seconds
I, [2020-03-02T21:36:12.616213 #1237]  INFO -- : ---> rank:1 finished processing
I, [2020-03-02T21:36:13.614584 #1238]  INFO -- : -----> rank:2 waiting until processing is finished
I, [2020-03-02T21:36:13.614596 #1239]  INFO -- : -------> rank:3 waiting until processing is finished
I, [2020-03-02T21:36:13.615954 #1237]  INFO -- : ---> rank:1 sending token to 0 with 0 count
I, [2020-03-02T21:36:13.616137 #1237]  INFO -- : ---> rank:1 received token 0 from 0
I, [2020-03-02T21:36:13.616172 #1237]  INFO -- : ---> rank:1 sending token to 0 with 1 count
I, [2020-03-02T21:36:13.616066 #1236]  INFO -- : -> rank:0 received token 0 from 1
I, [2020-03-02T21:36:14.614753 #1239]  INFO -- : -------> rank:3 finished processing
I, [2020-03-02T21:36:15.614645 #1238]  INFO -- : -----> rank:2 finished processing
I, [2020-03-02T21:36:15.614821 #1238]  INFO -- : -----> rank:2 sending token to 0 with 0 count
I, [2020-03-02T21:36:15.614989 #1236]  INFO -- : -> rank:0 received token 0 from 2
I, [2020-03-02T21:36:15.615056 #1238]  INFO -- : -----> rank:2 received token 0 from 0
I, [2020-03-02T21:36:15.615571 #1239]  INFO -- : -------> rank:3 sending token to 0 with 0 count
I, [2020-03-02T21:36:15.615925 #1236]  INFO -- : -> rank:0 received token 0 from 3
I, [2020-03-02T21:36:15.615710 #1238]  INFO -- : -----> rank:2 sending token to 0 with 1 count
I, [2020-03-02T21:36:15.616025 #1236]  INFO -- : -> rank:0 received token 1 from 1
I, [2020-03-02T21:36:15.616074 #1236]  INFO -- : -> rank:0 received token 1 from 2
I, [2020-03-02T21:36:15.616036 #1239]  INFO -- : -------> rank:3 received token 0 from 0
I, [2020-03-02T21:36:15.616195 #1238]  INFO -- : -----> rank:2 received token 1 from 0
I, [2020-03-02T21:36:15.616103 #1237]  INFO -- : ---> rank:1 received token 1 from 0
I, [2020-03-02T21:36:15.616322 #1236]  INFO -- : -> rank:0 received token 1 from 3
I, [2020-03-02T21:36:15.616146 #1239]  INFO -- : -------> rank:3 sending token to 0 with 1 count
I, [2020-03-02T21:36:15.616243 #1237]  INFO -- : ---> rank:1 sending token to 0 with 2 count
I, [2020-03-02T21:36:15.616373 #1236]  INFO -- : -> rank:0 received token 2 from 1
I, [2020-03-02T21:36:15.616413 #1239]  INFO -- : -------> rank:3 received token 1 from 0
I, [2020-03-02T21:36:15.616460 #1237]  INFO -- : ---> rank:1 received token 2 from 0
I, [2020-03-02T21:36:15.616623 #1239]  INFO -- : -------> rank:3 sending token to 0 with 2 count
I, [2020-03-02T21:36:15.616584 #1237]  INFO -- : ---> rank:1 sending token to 0 with 3 count
I, [2020-03-02T21:36:15.616709 #1238]  INFO -- : -----> rank:2 sending token to 0 with 2 count
I, [2020-03-02T21:36:15.616747 #1236]  INFO -- : -> rank:0 received token 2 from 2
I, [2020-03-02T21:36:15.616858 #1236]  INFO -- : -> rank:0 received token 2 from 3
I, [2020-03-02T21:36:15.617293 #1239]  INFO -- : -------> rank:3 received token 2 from 0
I, [2020-03-02T21:36:15.617092 #1238]  INFO -- : -----> rank:2 received token 2 from 0
I, [2020-03-02T21:36:15.617345 #1236]  INFO -- : -> rank:0 received token 3 from 1
I, [2020-03-02T21:36:15.617615 #1237]  INFO -- : ---> rank:1 received token 3 from 0
I, [2020-03-02T21:36:15.617676 #1237]  INFO -- : ---> rank:1 sending token to 0 with 4 count
I, [2020-03-02T21:36:15.617353 #1239]  INFO -- : -------> rank:3 sending token to 0 with 3 count
I, [2020-03-02T21:36:15.617952 #1238]  INFO -- : -----> rank:2 sending token to 0 with 3 count
I, [2020-03-02T21:36:15.618328 #1236]  INFO -- : -> rank:0 received token 3 from 2
I, [2020-03-02T21:36:15.618506 #1236]  INFO -- : -> rank:0 received token 3 from 3
I, [2020-03-02T21:36:15.618718 #1238]  INFO -- : -----> rank:2 received token 3 from 0
I, [2020-03-02T21:36:15.618779 #1238]  INFO -- : -----> rank:2 sending token to 0 with 4 count
I, [2020-03-02T21:36:15.618710 #1236]  INFO -- : -> rank:0 received token 4 from 1
I, [2020-03-02T21:36:15.618818 #1236]  INFO -- : -> rank:0 received token 4 from 2
I, [2020-03-02T21:36:15.619078 #1238]  INFO -- : -----> rank:2 received token 4 from 0
I, [2020-03-02T21:36:15.619130 #1238]  INFO -- : -----> rank:2 sending token to 0 with 5 count
I, [2020-03-02T21:36:15.618803 #1237]  INFO -- : ---> rank:1 received token 4 from 0
I, [2020-03-02T21:36:15.619055 #1237]  INFO -- : ---> rank:1 sending token to 0 with 5 count
I, [2020-03-02T21:36:15.618758 #1239]  INFO -- : -------> rank:3 received token 3 from 0
I, [2020-03-02T21:36:15.619503 #1239]  INFO -- : -------> rank:3 sending token to 0 with 4 count
I, [2020-03-02T21:36:15.619870 #1236]  INFO -- : -> rank:0 received token 4 from 3
I, [2020-03-02T21:36:15.619974 #1236]  INFO -- : -> rank:0 received token 5 from 1
I, [2020-03-02T21:36:15.620024 #1236]  INFO -- : -> rank:0 received token 5 from 2
I, [2020-03-02T21:36:15.620069 #1238]  INFO -- : -----> rank:2 received token 5 from 0
I, [2020-03-02T21:36:15.620030 #1237]  INFO -- : ---> rank:1 received token 5 from 0
I, [2020-03-02T21:36:15.620069 #1237]  INFO -- : ---> rank:1 sending token to 0 with 6 count
I, [2020-03-02T21:36:15.620611 #1239]  INFO -- : -------> rank:3 received token 4 from 0
I, [2020-03-02T21:36:15.620762 #1239]  INFO -- : -------> rank:3 sending token to 0 with 5 count
I, [2020-03-02T21:36:15.621058 #1239]  INFO -- : -------> rank:3 received token 5 from 0
I, [2020-03-02T21:36:15.620900 #1236]  INFO -- : -> rank:0 received token 5 from 3
I, [2020-03-02T21:36:15.620986 #1236]  INFO -- : -> rank:0 received token 6 from 1
I, [2020-03-02T21:36:15.621013 #1236]  INFO -- : -> rank:0 Termination detected!
```
