require 'mpi'
require 'logger'
require_relative 'helpers'

# message tags
PACKET = 0 # application message
TOKEN = 1 # token with the count of post-processing passive nodes

MPI.Init
world = MPI::Comm::WORLD
l = Logger.new(STDOUT)

# node starting the detection
if world.rank == 0
  termination_detected = false

  # send application message to all nodes
  world.size.times do |succ_i|
    next if succ_i == 0 # except yourself

    processing_time = rand(5)
    l.info "#{messsage_prefix(0)} sending activation packet to #{succ_i}"
    world.Send(processing_time.to_s, succ_i, PACKET)
  end

  # once jobs were dispatched, send token to all nodes
  send_token_to_all(world.rank, NArray.int(1), l)

  while termination_detected == false
    token_responses = {}
    world.size.times do |succ_i|
      next if succ_i == 0

      token_responses[succ_i] = {}
      token_responses[succ_i][:token] = NArray.int(1)
      token_responses[succ_i][:req] = world.Irecv(
        token_responses[succ_i][:token], succ_i, TOKEN
      )
    end

    token_responses.each do |id, token_response|
      token_response[:req].Wait
      l.info "#{messsage_prefix(0)} received token "\
             "#{token_response[:token][0]} from #{id}"

      if token_response[:token][0] == (world.size - 1) * 2
        termination_detected = true
        l.info "#{messsage_prefix(0)} Termination detected!"
        break
      end

      world.Send(token_response[:token], id, TOKEN) unless termination_detected
    end
  end
end

# other nodes
if world.rank != 0
  # init values
  colour = :white
  passive = true
  termination_detected = false

  packet_buffer = ' ' * 100
  world.Recv(packet_buffer, 0, PACKET)
  l.info "#{messsage_prefix(world.rank)} received packet from 0"
  processing_time = packet_buffer.strip.to_i
  colour = :black
  passive = false

  packet_receiver = Thread.new do
    l.info "#{messsage_prefix(world.rank)} starts processing "\
           "for: #{processing_time} seconds"
    sleep processing_time # processing is a sleep between 0-5 seconds
    l.info "#{messsage_prefix(world.rank)} finished processing"

    passive = true
  end

  token_receiver = Thread.new do
    while termination_detected == false
      token_buffer = NArray.int(1)
      world.Recv(token_buffer, 0, TOKEN)
      l.info "#{messsage_prefix(world.rank)} received token "\
             "#{token_buffer[0]} from 0"

      while passive == false
        l.info "#{messsage_prefix(world.rank)} waiting until "\
               'processing is finished'
        sleep 2 # this sleep only prevents from logs flood
      end

      token_buffer[0] = colour == :black ? 0 : token_buffer[0] + 1
      colour = :white

      termination_detected = true if token_buffer[0] == (world.size - 1) * 2

      l.info "#{messsage_prefix(world.rank)} sending token to 0 with "\
             "#{token_buffer[0]} count"
      world.Send(token_buffer, 0, TOKEN)
    end
  end

  [packet_receiver, token_receiver].each(&:join)
end

MPI.Finalize
