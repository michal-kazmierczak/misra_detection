include NumRu

def messsage_prefix(rank)
  "#{(rank * 2).times.map{ '-' }.join}-> rank:#{rank}"
end

def send_token_to_all(from, token, l)
  MPI::Comm::WORLD.size.times do |succ_i|
    next if succ_i == from # except yourself

    l.info "#{messsage_prefix(from)} sending token to #{succ_i} with #{token[0]} count"

    MPI::Comm::WORLD.Send(token, succ_i, TOKEN)
  end
end
